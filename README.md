![](https://assets.zeit.co/image/upload/v1527770721/repositories/serve/serve-repo-banner.png)
  
This is an docker image dedicated to running an instance of [zeit/serve](https://github.com/zeit/serve).  
  
## Usage
```
docker build -t zeit-serve "https://gitlab.com/mkrump/docker-zeit-serve.git"
docker create \
	--name zeit-serve \
	-v /your/path1:/serve/path1 \
	-v /your/path2:/serve/path2 \
	-e PGID=1000 -e PUID=1000 \
	-p 5000:5000 \
	zeit-serve
```
  
## Parameters
Here an short explanation of the paramters used.  
  
`-v /your/path1:/serve/path1` is used to mount a folder in the containers `/serve` path.  
This is the folder that will be made public. You can add as many path as you want here.  
  
`-e PGID=1000 -e PUID=1000` is used to avoid potential permission issues.  
Make sure the execute `id your-username` on the host to get your UID & GID.  
  
`-p 5000:5000` makes sure that the ports are forwarded from the container to the host.  
You can change the first value to use another port of the host system.  
  
## Updating
Simply re-run the build command, destroy and re-create the container.
