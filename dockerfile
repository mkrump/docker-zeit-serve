FROM node:lts-alpine3.15

# Install bash & serve
RUN apk add bash
RUN yarn global add serve

# Don't 404 if no volumes mounted
RUN mkdir /serve

EXPOSE 5000
CMD [ "serve", "/serve" ]
